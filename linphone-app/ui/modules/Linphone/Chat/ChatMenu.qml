import QtQuick 2.7
import QtQuick.Layouts 1.3

import Clipboard 1.0
import Common 1.0
import Linphone 1.0

import Common.Styles 1.0
import Linphone.Styles 1.0
import TextToSpeech 1.0
import Utils 1.0
import Units 1.0
import UtilsCpp 1.0
import LinphoneEnums 1.0

import 'Message.js' as Logic

// =============================================================================
// ChatMenu
Item {
	id: container
	property string lastTextSelected
	property string content
	property int deliveryCount : 0
	property bool deliveryVisible: false
	
	signal deliveryStatusClicked()
	signal removeEntryRequested()
	signal copyAllDone()
	signal copySelectionDone()

	function open(){
		messageMenu.popup()
	}
	
	
	Menu {
		id: messageMenu
		menuStyle : MenuStyle.aux
		MenuItem {
			//: 'Copy all' : Text menu to copy all message text into clipboard
			text: (container.lastTextSelected == '' ? qsTr('menuCopyAll')
													  //: 'Copy' : Text menu to copy selected text in message into clipboard
													:  qsTr('menuCopy'))
			iconMenu: MenuItemStyle.copy.icon
			iconSizeMenu: MenuItemStyle.copy.iconSize
			iconLayoutDirection: Qt.RightToLeft
			menuItemStyle : MenuItemStyle.aux
			onTriggered: {
				if( container.lastTextSelected == ''){
					Clipboard.text = container.content
					container.copyAllDone();
				}else{
					Clipboard.text = container.lastTextSelected
					container.copySelectionDone()
				}
			}
			visible: content != ''
		}
		
		MenuItem {
			enabled: TextToSpeech.available
			text: qsTr('menuPlayMe')
			iconMenu: MenuItemStyle.speaker.icon
			iconSizeMenu: MenuItemStyle.speaker.iconSize
			iconLayoutDirection: Qt.RightToLeft
			menuItemStyle : MenuItemStyle.aux
			onTriggered: TextToSpeech.say(container.content)
			visible: content != ''
		}
		MenuItem {
			//: 'Hide delivery status' : Item menu that lead to IMDN of a message
			text: (deliveryVisible ? qsTr('menuHideDeliveryStatus')
			//: 'Delivery status' : Item menu that lead to IMDN of a message
			: qsTr('menuDeliveryStatus')
			)
			iconMenu: MenuItemStyle.imdn.icon
			iconSizeMenu: MenuItemStyle.imdn.iconSize
			iconLayoutDirection: Qt.RightToLeft
			menuItemStyle : MenuItemStyle.aux
			visible: container.deliveryCount > 0
			onTriggered: container.deliveryStatusClicked()
		}
		MenuItem {
			//: 'Delete' : Item menu to delete a message
			text: qsTr('menuDelete')
			iconMenu: MenuItemStyle.deleteEntry.icon
			iconSizeMenu: MenuItemStyle.deleteEntry.iconSize
			iconLayoutDirection: Qt.RightToLeft
			menuItemStyle : MenuItemStyle.auxRed
			onTriggered: container.removeEntryRequested()
		}
	}
	
	
	
	// Handle hovered link.
	MouseArea {
		anchors.fill: parent
		//height: messageMenu.height
		//width: messageMenu.width
		
		acceptedButtons: Qt.RightButton
		propagateComposedEvents:true
		cursorShape: parent.hoveredLink
					 ? Qt.PointingHandCursor
					 : Qt.IBeamCursor
		onClicked: mouse.button === Qt.RightButton && messageMenu.popup()
	}
}
